public interface PizzaBuilder {

    public  void creerPizza();
    public  void buildName(String name);
    public  void buildSauce(String sauce);
    public  void buildPate(String pate);

    public  Pizza getResult();


}
