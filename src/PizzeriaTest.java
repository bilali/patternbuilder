public class PizzeriaTest {
    public static void main(String[] args) {
        PizzaBuilder pizzaBuilder = new ConcretePizzaBuilder();

        PizzaDirector pizzaDirector = new PizzaDirector(pizzaBuilder);

        pizzaDirector.constructPizza();
    }
}
