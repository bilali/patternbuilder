import java.util.ArrayList;

public class Pizza {
      String  name, sauce, pate;

     ArrayList ingrediens = new ArrayList();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public String getPate() {
        return pate;
    }

    public void setPate(String pate) {
        this.pate = pate;
    }

    public void preparer(){
        System.out.printf("Preparation de la pizza" );
        System.out.println("Preparation de la pate");
        System.out.println("Rajouter la sauce ...");
        System.out.println("Rajouter les ingredients:");

        for (int i =0; i<ingrediens.size(); i++ ) {
            System.out.println(" " + ingrediens.get(i));
        }
    }

    public void cuire(){
        System.out.println("Cuisson");
    }

    public void couper(){
        System.out.println("Decoupe");
    }

    public void emballer(){
        System.out.println("Emballage");
    }
}
