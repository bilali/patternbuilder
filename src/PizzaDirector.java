public class PizzaDirector {

    PizzaBuilder pizzaBuilder;

    public PizzaDirector(PizzaBuilder pizzaBuilder){

        this.pizzaBuilder = pizzaBuilder;

    }

    public Pizza constructPizza(){

        pizzaBuilder.creerPizza();
        pizzaBuilder.buildName("poulet");
        pizzaBuilder.buildPate("pate");
        pizzaBuilder.buildSauce("sauce");

        Pizza pizza = pizzaBuilder.getResult();

        pizza.preparer();
        pizza.cuire();
        pizza.couper();
        pizza.emballer();

        return  pizza;


    }
}
