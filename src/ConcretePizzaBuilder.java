public class ConcretePizzaBuilder implements PizzaBuilder {
    Pizza pizza;

    /*public  ConcretePizzaBuilder(){
        pizza = new Pizza();
    }*/

    public void creerPizza(){
        pizza = new Pizza();
    }

    @Override
    public void buildName(String name) {
        pizza.setName(name);
    }

    @Override
    public void buildSauce(String sauce) {
        pizza.setSauce(sauce);
    }

    @Override
    public void buildPate(String pate) {
        pizza.setPate(pate);
    }

    @Override
    public  Pizza getResult(){
        return  this.pizza;
    }
}
